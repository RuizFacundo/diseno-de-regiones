package controlador;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import datos.DatosJSON;
import modelo.Arista;
import modelo.Estadistica;
import modelo.Grafo;
import vista.MapaFrame;
import vista.AgregarPesosFrame;
import vista.EstadisticasFrame;
public class Interprete {

	public static void actualizarAristas(Grafo grafo,MapaFrame vista) {
		ArrayList<MapPolygon> nuevo= new ArrayList<MapPolygon>();
		vista.getMapa().setMapPolygonList(nuevo);		
		
		for(Arista a: grafo.getAristas()) {
			int i = a.getVertice1();
			int j = a.getVertice2();
			Coordinate c1 = null;
			Coordinate c2 = null;
			List<MapMarker> markers = vista.getMapa().getMapMarkerList();
			for(MapMarker m: markers) {
				if(Integer.parseInt(m.getName()) == i) 
					c1 = m.getCoordinate();

				if(Integer.parseInt(m.getName()) == j) 
					c2 = m.getCoordinate();

				if(c1!=null && c2!=null) {
					MapPolygonImpl camino = new MapPolygonImpl(c1,c2,c1);
					camino.setColor(new Color(86,180,164));
				vista.getMapa().addMapPolygon(camino);
					c1 = null;
					c2 = null;
				}
			}
		}
	}
	
	public static void cargarTabla(Grafo grafo,  DatosJSON datosJSon) {
		Object [][]datos = new Object[grafo.getAristas().size()][2];
		Map<Integer,String> vertices=datosJSon.verticesJSON();
		for(int i = 0; i<grafo.getAristas().size(); i++) {
			datos[i][0] = vertices.get(grafo.getAristas().get(i).getVertice1())+"-"+  vertices.get(grafo.getAristas().get(i).getVertice2());
			datos[i][1] = null;
		}
		AgregarPesosFrame.getTable().setModel(
			new DefaultTableModel(datos, new String[] {"CONEXI�N", "SIMILITUD"}) 
				{
			Class[] columnTypes = new Class[] {
					Object.class, Integer.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] canEdit = new boolean[]{
	                false, true
	        };
			public boolean isCellEditable(int row, int col) {
	            return canEdit[col];
	        }
		});
		AgregarPesosFrame.getTable().getColumnModel().getColumn(0).setPreferredWidth(184);
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		AgregarPesosFrame.getTable().getColumnModel().getColumn(0).setCellRenderer(tcr);
		AgregarPesosFrame.getTable().getColumnModel().getColumn(1).setCellRenderer(tcr);
	}
	
	public static void cargarPesosDeAristas(Grafo grafo){
		for(int i=0;i < AgregarPesosFrame.getTable().getRowCount();i++){
			grafo.getAristas().get(i).setPeso((Integer.parseInt(AgregarPesosFrame.getTable().getValueAt(i,1).toString())));
		}
	}
	
	public static void calcularEstadisticas(Estadistica estadisticas, Grafo grafo,EstadisticasFrame estadisticasFrame,
			DatosJSON coordenadasJSON,MapaFrame vista){
		
		Integer eliminadas =Integer.parseInt(vista.getCantRegiones().getText())-1;
		Long tiempoEjecucion=estadisticas.tiempoDeEjecucion(estadisticas.getTInicio(),estadisticas.getTFinal());
		Map<Integer,String> vertices= coordenadasJSON.verticesJSON();
		Arista menor = estadisticas.obtenerAristaMenor(grafo);
		Arista mayor = estadisticas.obtenerAristaMayor(grafo);
		String aristaMenor = vertices.get(menor.getVertice1()) + "-" + vertices.get(menor.getVertice2()) +
				", con una similitud de : " + menor.getPeso();
		String aristaMayor = vertices.get(mayor.getVertice1()) + "-" + vertices.get(mayor.getVertice2()) +
				", con una similitud de : " + mayor.getPeso();
		estadisticasFrame.mostrarEstadisticas(tiempoEjecucion, estadisticas.pesoPromedioAristas(grafo), 
				eliminadas, aristaMenor, aristaMayor);
		estadisticasFrame.mostrarVentana();	
	}

}
