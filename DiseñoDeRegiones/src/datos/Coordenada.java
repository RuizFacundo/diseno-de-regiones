package datos;

import java.awt.Color;
import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

public class Coordenada {

	private double latitud;
	private double longitud;
	private String nombre;
	
	
	public Coordenada(double latitud, double longitud, String nombre) {
		super();
		this.latitud = latitud;
		this.longitud = longitud;
		this.nombre = nombre;
	}
	
	public double getLatitud() {
		return latitud;
	}
	
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	
	public double getLongitud() {
		return longitud;
	}
	
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public static  ArrayList<MapMarker> castearAMarkers(ArrayList<Coordenada> coordenadas){
		ArrayList<MapMarker> markers = new ArrayList<MapMarker>();
		int cont = 0;
		for(Coordenada c: coordenadas) {
			String numVertice = String.valueOf(cont);
			Coordinate coordenada = new Coordinate(c.getLatitud(),c.getLongitud());
			MapMarkerDot marker =  new MapMarkerDot(numVertice,coordenada);
			cont++;
			marker.getStyle().setColor(new Color(245,70,125));
			marker.getStyle().setBackColor(new Color(252,210,66));
			markers.add(marker);
		}
		return markers;
	}

	
	
}
