package vista;

import java.io.File;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Musica {
	private static Clip sonido;
	
	
	public void musica() {
		try {
			setSonido(AudioSystem.getClip());
			getSonido().open(AudioSystem.getAudioInputStream(new File("resources/cancion.wav")));
			getSonido().start();
			getSonido().loop(Clip.LOOP_CONTINUOUSLY);
		} catch (Exception e) {
			System.out.println("" + e);
		}
	}

	public void apagarMusica() {
		getSonido().stop();
	}
	
	public Clip getSonido() {
		return sonido;
	}
	
	public void setSonido(Clip s) {
		sonido = s;
	}
}
