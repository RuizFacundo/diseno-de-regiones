package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import datos.DatosJSON;


public class Grafo {

	private ArrayList<Set<Integer>> vecinos;
	private int vertices;
	private ArrayList<Arista> aristas;

	public Grafo() {}

	public Grafo(int n){
		vecinos = new ArrayList<Set<Integer>>();
		for(int i=0 ; i < n ; i++) {
			vecinos.add(new HashSet<Integer>());
		}
		aristas = new ArrayList<Arista>();		
		vertices = n;
	}

	public ArrayList<Arista> getAristas() {
		return aristas;
	}

	public void agregarArista(int i,int j){
		verificarArista(i,j, "agregar");
		vecinos.get(i).add(j);
		vecinos.get(j).add(i);
		Arista arista = new Arista(i,j);
		aristas.add(arista);
	}

	public void eliminarArista(int i, int j){
		verificarArista(i,j,"eliminar");
		vecinos.get(i).remove(j);  
		vecinos.get(j).remove(i);
		Arista arista=obtenerArista(i, j);
		aristas.remove(arista);
	}

	private Arista obtenerArista(int i, int j) {
		for(Arista arista: aristas) {
			if (arista.getVertice1()==i && arista.getVertice2()==j ||(arista.getVertice1()==j && arista.getVertice2()==i)){
				return arista;
			}
		}
		return null;
	}

	public boolean existeArista(int i, int j){
		return vecinos.get(i).contains(j);
	}

	public void setPesoArista(int i, int j, int peso) {
		verificarArista(i,j,"acceder");
		obtenerArista(i,j).setPeso(peso);
	}

	public int getPesoArista(int i, int j) { 
		verificarArista(i,j,"acceder al peso de ");
		if(existeArista(i,j)) {
			return obtenerArista(i,j).getPeso();
		}
		return -1;
	}

	private void verificarArista(int i, int j, String tipo) {
		if (i == j) {
			throw new IllegalArgumentException("Se intento " + tipo + " una arista con i=j : "+i +"/"  + j);
		}
		verificarVertice(i, tipo + " un vertice ");
		verificarVertice(j, tipo + " un vertice ");
	}

	private void verificarVertice(int i, String tipo) {
		if (i < 0 || i >= cantidadVertices())
			throw new IllegalArgumentException("Se intento " + tipo + " un vertice con valores, fuera de rango: " + i);
	}

	public int cantidadVertices(){
		return vertices;
	}

	public void generarRegiones(int regiones){
		int i=0;
		Collections.sort(aristas);
		while(i < (regiones-1)){
			eliminarArista(aristas.get(0).getVertice1(),aristas.get(0).getVertice2());
			i++;
		}
	}

	public void armarGrafo(){
		DatosJSON datosJSON=new DatosJSON();
		datosJSON = datosJSON.leerJSON("resources/datos.JSON");
		ArrayList<Arista> aristasJSON=datosJSON.getAristasJSON();
		for(int i=0;i<aristasJSON.size();i++){
			agregarArista(aristasJSON.get(i).getVertice1(),aristasJSON.get(i).getVertice2());
		}
	}
	
	public Grafo construirNuevoGrafo(Grafo anterior,int k){
		ArbolGeneradorMinimo arbol= new ArbolGeneradorMinimo(anterior);
		Grafo grafoNuevo=arbol.generarArbol();
		grafoNuevo.generarRegiones(k);
		return grafoNuevo;
	}

	public ArrayList<Set<Integer>> getVecinos() {
		return vecinos;
	}

}
