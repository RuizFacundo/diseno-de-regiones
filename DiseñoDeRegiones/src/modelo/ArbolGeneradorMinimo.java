package modelo;

import java.util.Collections;

public class ArbolGeneradorMinimo{

	public static int[] representantesConexas;
	private static Grafo grafo;
	

	public ArbolGeneradorMinimo(Grafo g){
		grafo=g;
		representantesConexas = new int[g.cantidadVertices()];
		for(int i = 0; i<representantesConexas.length; i++) {
			representantesConexas[i] = i;
		}
	}
	
	public Grafo generarArbol() {
		Grafo grafoNuevo = new Grafo(grafo.cantidadVertices());
		int i = 1;
		int min = 1;
		Collections.sort(grafo.getAristas());
		while(i <= grafo.cantidadVertices() - 1) {
			Arista aristaMinima = grafo.getAristas().get(grafo.getAristas().size()-min);
			min++;
			if(!formaCiclo(aristaMinima)) {
				grafoNuevo.agregarArista(aristaMinima.getVertice1(), aristaMinima.getVertice2());
				grafoNuevo.setPesoArista(aristaMinima.getVertice1(), aristaMinima.getVertice2(), aristaMinima.getPeso());
				i++;
			}
		}
		return grafoNuevo;
	}

	public boolean formaCiclo(Arista aristaMinima) {	
			int i = aristaMinima.getVertice1();
			int j = aristaMinima.getVertice2();
			
			if(!find(i,j)) {
				union(i,j);
				return false;
			}
			else {
				return true;
			}
	}
	
	private int raiz(int i) {		
		if (representantesConexas[i] != i) 
			representantesConexas[i] = raiz(representantesConexas[i]);
		return representantesConexas[i];
	}
	
	private boolean find(int i, int j){
		return raiz(i) == raiz(j);
	}
	
	private void union(int i, int j){
		int ri = raiz(i);
		int rj = raiz(j);
		representantesConexas[rj] = ri;
	}
	
	public void setGrafo(Grafo grafo) {
		this.grafo = grafo;
	}
		
}