package vista;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;


import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;




public class MapaFrame {

	private JFrame frame;
	private static JMapViewer mapa;
	private JButton btnAgregarPesos;
	private JButton btnGenerar;
	private JButton btnEstadisticas;
	private JTextField cantRegiones;
	private ArrayList<MapMarker> MarkersDots;



	public MapaFrame() {
		super();
		initialize();
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Louis George Caf�", Font.BOLD, 16));
		frame.setBounds(100, 50, 700, 750);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);	
		frame.getContentPane().setBackground(new Color(230,250,255));
		frame.setResizable(false);
		JPanel panel = new JPanel();
		panel.setBounds(120,40, 434,520);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		frame.setResizable(false);
		crearBotonesYComponentes(panel);
		crearFondo();
	}

	private void crearBotonesYComponentes(JPanel panel) {
		JLabel lblRegiones = new JLabel("Cantidad de Regiones: ");
		lblRegiones.setForeground(Color.DARK_GRAY);
		lblRegiones.setFont(new Font("Calibri", Font.BOLD, 16));
		lblRegiones.setBounds(193,581, 169, 25);
		
		frame.getContentPane().add(lblRegiones);
		
		cantRegiones = new JTextField();
		cantRegiones.setBounds(372, 580, 100, 23);
		frame.getContentPane().add(cantRegiones);
		
		btnAgregarPesos = new JButton("Agregar Pesos");
		btnAgregarPesos.setIcon(new ImageIcon(this.getClass().getResource("/btnGuardarPesos.jpg")));
		btnAgregarPesos.setBounds(193,614,145, 36);
		frame.getContentPane().add(btnAgregarPesos);
		
		btnGenerar = new JButton("");
		btnGenerar.setIcon(new ImageIcon(this.getClass().getResource("/btnGenerar.jpg")));
		btnGenerar.setBounds(360, 614, 130, 36);
		frame.getContentPane().add(btnGenerar);
		
		JMapViewer mapa= crearMapa();
		panel.add(mapa);
		
		JList list = new JList();
		list.setBounds(86, 622, 1, 1);
		frame.getContentPane().add(list);
		
		btnEstadisticas = new JButton("");
		btnEstadisticas.setBounds(601, 641, 56, 45);
		btnEstadisticas.setIcon(new ImageIcon(this.getClass().getResource("/btnEstadisticas.jpg")));
		frame.getContentPane().add(btnEstadisticas);
	}

	private JMapViewer crearMapa() {
		mapa = new JMapViewer();
		mapa.setZoomControlsVisible(false);
		mapa.setAutoscrolls(false);
		mapa.setBounds(0, 0, 700, 700);

		Coordinate coordinate = new Coordinate(-46.0639555946604, -50.5888671875);
		mapa.setDisplayPosition(coordinate, 4);
		mapa.setLayout(null);
		return mapa;
	}
	
	private void crearFondo() {
		JLabel fondo = new JLabel();
	    fondo.setBounds(0, 0, 694, 721);
		fondo.setIcon(new ImageIcon(this.getClass().getResource("/fondoMapa.jpg")));
		frame.getContentPane().add(fondo);	
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estas seguro que quieres salir ?", 
		             "Confirmaci�n", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
		
	public void alertaRegiones() {
		JOptionPane.showMessageDialog(frame, "Debe ingresar una cantidad de Regiones v�lida!");
	}
	
	public void alertaEstadistica() {
		JOptionPane.showMessageDialog(frame, "Debe generar las regiones primero!");
		
	}
	
	public JButton getBtnAgregarPesos() {
		return btnAgregarPesos;
	}

	public JButton getBtnGenerar() {
		return btnGenerar;
	}

	public JTextField getCantRegiones() {
		return cantRegiones;
	}

	public void setCantRegiones(JTextField cantRegiones) {
		this.cantRegiones = cantRegiones;
	}

	public ArrayList<MapMarker> getMarkersDot() {
		return this.MarkersDots;
	}

	public void setMarkers(ArrayList<MapMarker> markers) {
		this.MarkersDots = markers;
	}
	
	public JMapViewer getMapa() {
		return mapa;
	}
	
	public JFrame getFrame() {
		return this.frame;
	}

	public JButton getBtnEstadisticas() {
		return btnEstadisticas;
	}

}

