package modelo;

import java.util.Collections;

public class Estadistica {
	
	private Long TInicio;
	private Long TFinal;
	private Long tiempoEjecucion;

	
	public double pesoPromedioAristas(Grafo grafo) {
		int promedio=0;
		for(int i=0;i<grafo.getAristas().size(); i++){
			promedio= promedio + grafo.getAristas().get(i).getPeso();
		}
		return promedio/grafo.getAristas().size();
	}
	
	public Long tiempoDeEjecucion(Long TInicio, Long TFinal){
		tiempoEjecucion=TFinal-TInicio;
		return tiempoEjecucion;
	}
	
	public Arista obtenerAristaMayor(Grafo grafo){
		Collections.sort(grafo.getAristas());
		return grafo.getAristas().get(0);
	}
	
	public Arista obtenerAristaMenor(Grafo grafo){
		return Collections.max(grafo.getAristas());
	}
	
	public void setTiempoEjecucion(Long tiempoEjecucion) {
		this.tiempoEjecucion = tiempoEjecucion;
	}

	public Long getTInicio() {
		return TInicio;
	}

	public void setTInicio(Long tInicio) {
		TInicio = tInicio;
	}

	public Long getTFinal() {
		return TFinal;
	}

	public void setTFinal(Long tFinal) {
		TFinal = tFinal;
	}
	
}
