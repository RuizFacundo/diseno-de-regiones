package datos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import modelo.Arista;


public class DatosJSON {
	
	private ArrayList<Coordenada> coordenadas;
	private Map<Integer,String> vertices;
	private ArrayList<Arista> aristas;
	
	public DatosJSON(){
		coordenadas = new ArrayList<Coordenada>();
		vertices=new HashMap<Integer,String>();
		aristas= new ArrayList<Arista>();
	}
	
	public String generarJSON() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		return json;
		
	}
	
	public void guardarJSON(String jsonParaGuardar, String archivoDestino) {	
		try {
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public DatosJSON leerJSON(String Archivo) {
		Gson gson = new Gson();
		DatosJSON nuevo = null;
		try {
			BufferedReader br = new BufferedReader( new FileReader(Archivo));
			nuevo = gson.fromJson(br, DatosJSON.class);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		return nuevo;
	}
	
	public ArrayList<Coordenada> getCoordenadasJSON() {
		return coordenadas;
	}
	
	public ArrayList<Arista> getAristasJSON() {
		return aristas;
	}
	
	public Map <Integer,String> verticesJSON(){
		return vertices;
	}
	
	public void addCoord(Coordenada c) {
		coordenadas.add(c);
	}
	
	public void addVertice(Integer numVertice, String nombre) {
		vertices.put(numVertice,nombre);
	}
	
	public void addArista(Arista arista) {
		aristas.add(arista);
	}
	
}

	

	
