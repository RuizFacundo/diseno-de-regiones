package vista;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

public class EstadisticasFrame extends JFrame {
	
	private static final long serialVersionUID = 2506894226932486272L;
	private static EstadisticasFrame INSTANCE;
	private JPanel contentPane;
	private JButton btnVolver;
	private JLabel tiempo;
	private JTextArea pesos;
	private JLabel eliminadas;
	private JLabel titulo;
	private JTextArea menor;
	private JTextArea mayor;
	
	public static EstadisticasFrame getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new EstadisticasFrame(); 	
			return new EstadisticasFrame();
		}
		else
			return INSTANCE;
	}
	
	private EstadisticasFrame() 
	{
		super();

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 600, 650);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.scrollbar);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);	
		setResizable(false);
		inicializarComponentes();
		crearFondo();
		
		this.setVisible(false);
	}	
	
	private void inicializarComponentes() {
		btnVolver= new JButton("");
		btnVolver.setIcon(new ImageIcon(this.getClass().getResource("/btnVolver.jpg")));
		btnVolver.setBounds(229,515, 144, 36);
		getContentPane().add(btnVolver);
		
		titulo = new JLabel("Estad�sticas");
		titulo.setFont(new Font("Louis George Caf�", Font.BOLD, 50));
		titulo.setForeground(new Color(86,180,164));
		titulo.setBounds(157, 74, 320, 46);
		contentPane.add(titulo);
		
		tiempo = new JLabel("");
		tiempo.setForeground(Color.GRAY);
		tiempo.setBounds(44, 156, 550, 30);
		tiempo.setFont(new Font("Louis George Caf�", Font.BOLD, 20));
		contentPane.add(tiempo);
		
		pesos = new JTextArea("");
		pesos.setForeground(Color.GRAY);
		pesos.setBounds(44, 212, 550, 58);
		pesos.setFont(new Font("Louis George Caf�", Font.BOLD, 20));
		contentPane.add(pesos);
		
		
		eliminadas = new JLabel("");
		eliminadas.setForeground(Color.GRAY);
		eliminadas.setBounds(44, 275, 550, 30);
		eliminadas.setFont(new Font("Louis George Caf�", Font.BOLD, 20));
		contentPane.add(eliminadas);
		
		menor = new JTextArea("");
		menor.setForeground(Color.GRAY);
		menor.setBounds(44, 330, 550, 58);
		menor.setFont(new Font("Louis George Caf�", Font.BOLD, 20));
		contentPane.add(menor);
		
		mayor = new JTextArea("");
		mayor.setForeground(Color.GRAY);
		mayor.setBounds(44, 411, 550, 64);
		mayor.setFont(new Font("Louis George Caf�", Font.BOLD, 20));
		contentPane.add(mayor);
		
		
	}

	public void mostrarEstadisticas(Long tiempoEje, Double promedioPesosAristas, int aristasEliminadas,String aristaMenor, String aristaMayor){
		tiempo.setText("El tiempo de ejecuci�n fue de: "+tiempoEje + " milisegundos!");
		pesos.setText("El promedio de la similaridad entre provincias es de: \n" + promedioPesosAristas + " !");
		eliminadas.setText("La cantidad de conexiones eliminadas es: " + aristasEliminadas + " !");
		menor.setText("Las provincias con menor similaridad son: \n" + aristaMenor+ " !");
		mayor.setText("Las provincias con mayor similaridad son: \n"+ aristaMayor+ " !");
	}
	
	private void crearFondo() {
		JLabel fondo = new JLabel();
	    fondo.setBounds(-46, -13, 668, 658);
	    
		fondo.setIcon(new ImageIcon(this.getClass().getResource("/fondoPesos.jpg")));
		contentPane.add(fondo);	
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}

	public void cerrarVentana() {
		this.dispose();
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}
}