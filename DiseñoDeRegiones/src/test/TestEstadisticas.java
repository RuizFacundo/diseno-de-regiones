package test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import modelo.Arista;
import modelo.Estadistica;
import modelo.Grafo;

public class TestEstadisticas {
	
	Grafo grafo;
	Estadistica estadistica;
	
	@Before
	public void setUp() throws Exception{
		grafo = new Grafo(3);
		estadistica = new Estadistica();
		grafo.agregarArista(0, 1);
		grafo.agregarArista(2, 1);
		grafo.agregarArista(0, 2);
		grafo.setPesoArista(0, 2, 3);
		grafo.setPesoArista(0, 1, 1);
		grafo.setPesoArista(2, 1, 2);
	}

	@Test
	public void pesoPromedioAristasTest() {
		assertTrue(estadistica.pesoPromedioAristas(grafo)==2);
	}
	
	@Test
	public void obtenerAristaMayor() {
		Arista esperada = new Arista(0,2);
		assertEquals(esperada, estadistica.obtenerAristaMayor(grafo));
	}
	
	@Test
	public void obtenerAristaMenor() {
		Arista esperada = new Arista(0,1);
		assertEquals(esperada, estadistica.obtenerAristaMenor(grafo));
	}
	
}