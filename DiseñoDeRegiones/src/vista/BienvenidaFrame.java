package vista;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;

public class BienvenidaFrame {

	private JPanel contentPane;
	private JButton btnIniciar;
	private JFrame frame;
	
	public BienvenidaFrame() {
		frame = new JFrame();
		frame.getContentPane().setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.white);
		frame.setBounds(334, 0, 700, 750);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		frame.setContentPane(contentPane);
		frame.setResizable(false);
		crearBoton();
		crearFondo();
	}

	public void mostrarVentana(){
		frame.setVisible(true);
	}

	private void crearFondo() {
		JLabel fondo = new JLabel();
	    fondo.setBounds(0, 0, 684, 711);
		fondo.setIcon(new ImageIcon(this.getClass().getResource("/fondoBienvenida.jpg")));
		frame.getContentPane().add(fondo);	
	}
	
	private void crearBoton() {
		btnIniciar = new JButton("");
		btnIniciar.setIcon(new ImageIcon(this.getClass().getResource("/btnIniciar.jpg")));
		contentPane.add(btnIniciar, BorderLayout.SOUTH);
		btnIniciar.setFont(new Font("Segoe UI Black", Font.PLAIN, 31));
		btnIniciar.setBounds(219, 647, 250, 45);
		frame.getContentPane().add(btnIniciar);
	}

	public JButton getBtnIniciar() {
		return this.btnIniciar;
	}

	public void cerrarVentana() {
		frame.dispose();
	}
	

}