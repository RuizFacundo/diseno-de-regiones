package test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import modelo.Grafo;


public class TestGrafo {
	
	Grafo grafo;
	
	@Before
	public void setUp() throws Exception{
		grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(2, 1);
		grafo.agregarArista(2, 0);
		grafo.agregarArista(3, 1);
		grafo.setPesoArista(0, 1, 7);
		grafo.setPesoArista(2, 1, 4);
		grafo.setPesoArista(2, 0, 9);
		grafo.setPesoArista(3, 1, 2);
		
	}
	
	@Test
	public void agregarAristaExistenteTest() {
		grafo.agregarArista(0, 2);
		assertTrue(grafo.existeArista(0, 2));
	}

	@Test (expected = IllegalArgumentException.class)
	public void agregarLoopTest() {
		grafo.agregarArista(2, 2);
	}
	
	@Test
	public void eliminarAristaExistenteTest() {
		grafo.eliminarArista(0, 1);
		assertFalse(grafo.existeArista(0, 1));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarAristaInexistenteTest() {
		grafo.eliminarArista(0, 6);
	}

	@Test (expected = IllegalArgumentException.class)
	public void setPesoAristaInexistente() {
		grafo.setPesoArista(0, 6, 8);;
	}
	
	@Test
	public void setPesoAristaExistente(){
		grafo.setPesoArista(0, 1, 4);
		assertTrue(grafo.getPesoArista(0, 1) == 4);
	}
	
	@Test
	public void aristaOpuestaTest(){
		grafo.agregarArista(2, 3);
		assertTrue(grafo.existeArista(3, 2));
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoSuperior(){
		Grafo g = new Grafo(5);
		g.agregarArista(0, 5);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoInferior(){
		Grafo g = new Grafo(5);
		g.agregarArista(-1, 0);
	}
	
	@Test
	public void generarRegionesTest() {
		grafo.generarRegiones(2);
		assertFalse(grafo.existeArista(2,0)); //arista 2-0 arista mayor.
	}
	
	@Test
	public void construirNuevoGrafoTest() {
		grafo = grafo.construirNuevoGrafo(grafo, 2);
		Grafo grafoEsperado = new Grafo(4);
		grafoEsperado = grafoEjemplo();
		Assert.iguales(grafoEsperado,grafo);
	}
	
	private Grafo grafoEjemplo() {
		Grafo g = new Grafo(4);
		g.agregarArista(3, 1);
		g.agregarArista(2, 1);
		
		g.setPesoArista(3, 1, 2);
		g.setPesoArista(2, 1, 4);
		
		return g;
		
	}

}
