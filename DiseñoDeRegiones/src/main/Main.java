package main;

import controlador.Controlador;
import modelo.Grafo;
import vista.MapaFrame;

public class Main {

	public static void main(String[] args) {
		MapaFrame vista = new MapaFrame();	
		Grafo modelo = new Grafo();
		Controlador controlador = new Controlador(vista, modelo);
		controlador.inicializar();
	}
}

