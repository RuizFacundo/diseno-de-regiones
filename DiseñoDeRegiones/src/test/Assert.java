package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import modelo.Arista;
import modelo.Grafo;

public class Assert {
	
	static boolean iguales;
	
	public static void iguales(Grafo g1, Grafo g2) {
		assertEquals(g1.getAristas().size() , g2.getAristas().size());
		for(Arista arista1 : g1.getAristas()) {
			assertTrue(g2.getAristas().contains(arista1));
		}
	}
}