package test;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import modelo.ArbolGeneradorMinimo;
import modelo.Arista;
import modelo.Grafo;

public class TestArbolGeneradorMinimo {
	
	Grafo grafo;
	ArbolGeneradorMinimo arbol;
	
	@Before
	public void setUp() throws Exception{
		grafo = new Grafo(4);
		grafo.agregarArista(1,0);
		grafo.agregarArista(2,1);
		grafo.agregarArista(0,2);
		grafo.agregarArista(1,3);
		grafo.agregarArista(2,3);
		
		
		grafo.setPesoArista(1, 0, 4);
		grafo.setPesoArista(2, 1, 8);
		grafo.setPesoArista(0, 2, 3);
		grafo.setPesoArista(3, 2, 1);
		grafo.setPesoArista(3, 1, 2);
		
		arbol = new ArbolGeneradorMinimo(grafo);
		grafo = arbol.generarArbol(); 
	}
	
	@Test
	public void generarArbolTest() {
		grafoEsperado();	
		Assert.iguales(grafoEsperado() , grafo);
	}
	
	private Grafo grafoEsperado() {
		Grafo nuevo  = new Grafo(4);
		nuevo.agregarArista(0, 2);
		nuevo.agregarArista(2, 3);
		nuevo.agregarArista(3, 1);
		
		nuevo.setPesoArista(0, 2, 3);
		nuevo.setPesoArista(3, 2, 1);
		nuevo.setPesoArista(3, 1, 2);
		return nuevo;
	} 
	
	@Test
	public void formaCicloTest() { 
		Arista arista = new Arista(0,3);
		assertTrue(arbol.formaCiclo(arista));
	}
	
	@Test
	public void noFormaCicloTest() {
		Arista arista = new Arista(0,3);
		grafo.eliminarArista(1, 3);
		arbol = new ArbolGeneradorMinimo(grafo);
		assertFalse(arbol.formaCiclo(arista));
	}
	
}

