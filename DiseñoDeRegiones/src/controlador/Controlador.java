package controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import datos.Coordenada;
import datos.DatosJSON;
import modelo.Estadistica;
import modelo.Grafo;
import vista.AgregarPesosFrame;
import vista.BienvenidaFrame;
import vista.EstadisticasFrame;
import vista.MapaFrame;
import vista.Musica;

public class Controlador {
	
	private MapaFrame vista;
	private Grafo grafo;
	private DatosJSON coordenadasJSON;
	private AgregarPesosFrame agregarPesos;
	private Musica cancion;
	private BienvenidaFrame bienvenido;
	private Estadistica estadisticas;
	private EstadisticasFrame estadisticasFrame;
	private boolean estadisticaHabilitada;
	
	public Controlador(MapaFrame vista, Grafo modelo) {
		this.grafo = modelo;
		this.coordenadasJSON = new DatosJSON();
		this.cancion= new Musica();
		this.vista = vista;
		this.estadisticas= new Estadistica();
		
		this.vista.getBtnAgregarPesos().addActionListener(a -> agregarPesos(a));
		this.vista.getBtnGenerar().addActionListener(e -> generar(e));

		this.agregarPesos = AgregarPesosFrame.getInstance();
		this.agregarPesos.getBtnGuardarPesos().addActionListener(p->guardarPesos(p));
		this.agregarPesos.getBtnLimpiarPesos().addActionListener(j ->limpiarTabla(j));
		
		this.bienvenido= new BienvenidaFrame();
		this.bienvenido.getBtnIniciar().addActionListener(t->iniciar(t));
		
		this.estadisticasFrame = EstadisticasFrame.getInstance();
		this.vista.getBtnEstadisticas().addActionListener(b -> verEstadisticas(b));
		this.estadisticasFrame.getBtnVolver().addActionListener(v -> volver(v));
	}

	public void inicializar() {
		bienvenido.mostrarVentana();
	}

	public void agregarPesos(ActionEvent a) {
		this.agregarPesos.mostrarVentana();
	}

	private void iniciar(ActionEvent t) {
		bienvenido.cerrarVentana();
		cancion.musica();
		cargarGrafo();
		traerCoordenadas();
		actualizarAristas();
		cargarTabla();
		vista.show();
	}

	private void limpiarTabla(ActionEvent j) {
		agregarPesos.limpiartabla();
	}
		
	private void guardarPesos(ActionEvent p) {
		cargarGrafo();
		if(agregarPesos.tablaLlena() && !agregarPesos.poseeNumerosNegativos()){
			cargarPesosDeAristas();
			this.agregarPesos.cerrar();
		}
	}
	
	public void generar(ActionEvent e) {
		if(agregarPesos.tablaLlena() && regionesValidas()) {
			estadisticas.setTInicio(System.currentTimeMillis());
			int k = Integer.parseInt(vista.getCantRegiones().getText());
			grafo=grafo.construirNuevoGrafo(grafo,k);
			actualizarAristas();
			cargarGrafo();
			cargarPesosDeAristas();
			estadisticas.setTFinal(System.currentTimeMillis());
			habilitarEstadisticas();
		} 
		else {
			mensajeAlerta();
		}
	}

	private void mensajeAlerta() {
		if(!agregarPesos.tablaLlena() && !regionesValidas()) {
			agregarPesos.alertaTabla();
			vista.alertaRegiones();
		}
		else if(!regionesValidas()) {
			vista.alertaRegiones();
		}
		else{
			agregarPesos.alertaTabla();
		}
	}
	
	private void habilitarEstadisticas() {
		this.estadisticaHabilitada=true;
	}

	public void traerCoordenadas() {
		coordenadasJSON = coordenadasJSON.leerJSON("resources/datos.JSON");
		ArrayList<MapMarker> markers = Coordenada.castearAMarkers(coordenadasJSON.getCoordenadasJSON());
		vista.getMapa().setMapMarkerList(markers); 
	}

	public void actualizarAristas() {
		Interprete.actualizarAristas(grafo,vista);
	}
	
	private void cargarGrafo() {
		this.grafo = new Grafo(23);
		this.grafo.armarGrafo();
	}
	
	private void cargarPesosDeAristas(){
		Interprete.cargarPesosDeAristas(grafo);
	}
	
	public void cargarTabla() {
		Interprete.cargarTabla(grafo,coordenadasJSON);
	}

	private boolean regionesValidas(){
		if(vista.getCantRegiones().getText().matches("[0-9]+")) {
			Integer cant = Integer.parseInt(vista.getCantRegiones().getText());
			Boolean dentroRango = (cant > 0 && cant <= grafo.cantidadVertices());
			return dentroRango;
		}
		return false;
	}
	
	private void verEstadisticas(ActionEvent b) {
		if(estadisticaHabilitada){
			Interprete.calcularEstadisticas(estadisticas, grafo, estadisticasFrame, coordenadasJSON, vista);
		}else {
			vista.alertaEstadistica();
		}
	}
	
	private void volver(ActionEvent v) {	
		this.estadisticasFrame.cerrarVentana();	
	}
	
}