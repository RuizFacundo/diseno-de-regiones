package modelo;

public class Arista implements Comparable<Arista> {
	
	private int vertice1;
	private int vertice2;
	private int peso;
	
	public Arista() {
		
	}
	
	public Arista(int vertice1, int vertice2) {
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
	}
	
	public int getVertice1() {
		return vertice1;
	}
	
	public void setVertice1(int vertice1) {
		this.vertice1 = vertice1;
	}
	
	public int getVertice2() {
		return vertice2;
	}
	
	public void setVertice2(int vertice2) {
		this.vertice2 = vertice2;
	}
	
	public int getPeso() {
		return peso;
	}
	
	public void setPeso(int peso) {
		this.peso = peso;
	}

	@Override
	public int compareTo(Arista OtraArista) {
		if(this.peso < OtraArista.peso)
			return 1;
		else if(this.peso > OtraArista.peso) 
			return -1;
		
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		
		if (vertice1 == other.vertice1 && vertice2 == other.vertice2)
			return true;
		if(vertice1 == other.vertice2 && vertice2 == other.vertice1)
			return true;
		
		return false;
	}
	
	
	
	
}
