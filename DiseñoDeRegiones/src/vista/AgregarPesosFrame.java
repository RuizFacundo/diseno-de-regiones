package vista;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;

import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.Font;
import javax.swing.border.LineBorder;


public class AgregarPesosFrame extends JFrame{

	private static final long serialVersionUID = -2461486475639761432L;
	private JPanel contentPane;
	private JButton btnGuardarPesos;
	private JButton btnLimpiarTabla;
	private static AgregarPesosFrame INSTANCE;
	private static JTable table;


	public static AgregarPesosFrame getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new AgregarPesosFrame(); 	
			return new AgregarPesosFrame();
		}
		else
			return INSTANCE;
	}

	private AgregarPesosFrame() 
	{
		super();

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 600, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);	
		setResizable(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setOpaque(false);
		scrollPane.setForeground(Color.RED);
		scrollPane.setBounds(50,20, 480, 500);
		getContentPane().add(scrollPane);

		crearTabla(scrollPane);
		
		btnGuardarPesos = new JButton("");
		btnGuardarPesos.setIcon(new ImageIcon(this.getClass().getResource("/btnGuardar.jpg")));
		btnGuardarPesos.setBounds(314, 542, 144, 38);
		getContentPane().add(btnGuardarPesos);
		
		btnLimpiarTabla = new JButton("");
		btnLimpiarTabla.setIcon(new ImageIcon(this.getClass().getResource("/btnLimpiar.jpg")));
		btnLimpiarTabla.setBounds(140,542, 144, 38);
		getContentPane().add(btnLimpiarTabla);
		
		crearFondo();
		
		this.setVisible(false);
	}

	private void crearTabla(JScrollPane scrollPane) {
		table = new JTable();
		table.setBorder(new LineBorder(new Color(86,180,164)));
		table.setGridColor(new Color(86,180,164));
		scrollPane.setViewportView(table);
		table.setFont(new Font("Calibri", Font.BOLD, 17));
		table.getTableHeader().setFont(new Font("Calibri", Font.BOLD, 18));
		table.setBackground(Color.white);
		table.setForeground(Color.gray);
		table.getTableHeader().setBackground(new Color(86,180,164));
		table.getTableHeader().setForeground(Color.white);
		table.getTableHeader().setReorderingAllowed(false);
	}
	
	private void crearFondo() {
		JLabel fondo = new JLabel();
	    fondo.setBounds(0, 0, 594, 621);
		fondo.setIcon(new ImageIcon(this.getClass().getResource("/fondoPesos.jpg")));
		getContentPane().add(fondo);	
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}
	
	public void cerrar(){
		this.dispose();
	}
	
	public boolean tablaLlena() {
		for(int i=0; i < table.getRowCount(); i++){	
			if(table.getValueAt(i,1) == null){
				return false;
			}
		}
		return true;
	}
	
	public void limpiartabla(){
		for(int i=0;i < table.getRowCount();i++) {
			table.setValueAt(null, i, 1);
		}
	}
	
	public void alertaTabla() {
		JOptionPane.showMessageDialog(contentPane, "Debe completar la tabla!");
	}
	
	public void alertaNumerosNegativos() {
		JOptionPane.showMessageDialog(contentPane, "La similitud entre provincias debe ser un n�mero mayor o igual a cero!");
	}
	
	public boolean poseeNumerosNegativos(){
		for(int i=0; i < AgregarPesosFrame.getTable().getRowCount(); i++){
			if(Integer.parseInt(AgregarPesosFrame.getTable().getValueAt(i,1).toString()) < 0){
				alertaNumerosNegativos();
				return true;
			}
		}
		return false;
	}
	public JButton getBtnGuardarPesos() {
		return btnGuardarPesos;
	}
	
	public JButton getBtnLimpiarPesos() {
		return btnLimpiarTabla;
	}
	
	public static JTable getTable(){
		return table;
	}
}
